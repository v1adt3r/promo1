<?php

$req = $_POST;
$action = $req['action'];

//var_dump($_POST);

$time = date("Y-m-d H:i:s");

$email = "info@palla.su";
//$email = "isnapix@gmail.com";

if ($action == 'callback'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Перезвоните мне!", $email);
}

if ($action == 'consult'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Заявка на консультацию!", $email);
}

if ($action == 'catalog'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Заявка на каталог", $email);
}

if ($action == 'special'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Форма 'Специальные условия'", $email);
}

if ($action == 'cooperation'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Форма 'Начать сотрудничество'", $email);
}

if ($action == 'form'){
    $message = $req['message'];
    sendMail($message, "Лендинг - Форма 'Остались вопросы'", $email);
}

function sendMail($mes,$subj,$address){
    $to = $address;
    $subject = $subj;
    $message = "<b>".$subj."</b><br><small>создано ".date("d.m.y \в G:i")."</small><br><hr><br>".$mes;

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-Type: text/html; charset="utf-8"'."\n";
    $headers .= 'From: robot@palla.su' . "\r\n" .
        'Cc: tatyana@palla.su'. "\r\n" .
        'Reply-To: info@palla.su' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    if (mail($to, $subject, $message, $headers)) {
       $response['data'] = "Сообщение принято к отправке";
        $response['to'] = $address;
    } else {
        $response['data'] = "ошибка!";
        echo "Произошла ошибка - сообщение не отправлено".error_get_last();
    }

    $response['response'] = 'OK';
    addRequestToDatabase($subj,$mes,'Landing.palla.su');

    @header("Content-Type : application/json");
    header("Content-Type: text/html; charset='utf-8'");

}


function addRequestToDatabase($header,$message,$source){
    $db_name = "u0040958_default";
    $db_user = "u0040958_default";
    $db_password = "u00S4l09P5j";
    $db_host = "localhost";

    $connector = new PDO("mysql:host=$db_host;dbname=$db_name",$db_user,$db_password);
    $connector->query('SET NAMES `UTF8`');

    $date = date("Y-m-d H:i:s");

    $message = strip_tags($message, '<br></br>');
    $sql = "INSERT INTO `form_requests` (`header`,`content`,`date`,`source`) VALUES (?,?,?,?)";
    $query = $connector->prepare($sql);
    $result = $query->execute(array($header,$message,$date,$source));
}
?>