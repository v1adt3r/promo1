function sendAjax(action, _data, sender){

    var message = _data;
    var url = new URL(window.location.href);

    var utmText = '';

    try {
        utmText =  '<br>utm_source: <strong>' + url.searchParams.get("utm_source") + '</strong><br>';
        utmText += 'utm_term: <strong>' + url.searchParams.get("utm_term") + '</strong><br>';
        utmText += 'utm_campaign: <strong>' + url.searchParams.get("utm_campaign ") + '</strong><br>';
        utmText += 'utm_content: <strong>' + url.searchParams.get("utm_content ") + '</strong><br>';
        utmText += 'utm_term: <strong>' + url.searchParams.get("utm_term ") + '</strong><br>';
        utmText += 'source: <strong>' + url.searchParams.get("source ") + '</strong><br>';
    } catch (e){
        console.log(e.message);
    }

    message = message += utmText;

    var _response;
    $.ajax({
        type: 'POST',
        url: 'engine/engine.php',
        data: {
            action: action,
            message : message
        },
        success: function(data){
            if (sender) $(sender).html('Ваша заявка принята!');
            $('#responder').addClass('active');
        },
        timeout: function(){
            _response = 0;
        },
        error: function(){
            _response = 0;
        }
    });
    return _response;
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


$(document).ready(function(){
    $('#dissmissResponder').click(function(){
        $('#responder').removeClass('active');
    });

    $('input[type="tel"]').mask('+7 (999) 999-99-99');
    $('input').focus(function(){
       $(this).parent().removeClass('has-error');
    });

    $('#dismiss_responder').click(function(){
        $('#responder').removeClass('active');
    });

    $('.form-cooperation .sender, .form-consult .sender, .form-special .sender').click(function(){
        var validated = true;

        var name = $(this).parent().find("input.name");
        var email = $(this).parent().find("input.email");
        var phone = $(this).parent().find("input.phone");
        var tip = $(this).attr('data-value-id');

        var message = "";
        //console.log(phone);
        if ( $(name).val() == undefined || $(name).val() == "") { $(name).parent().addClass('has-error'); $(name).tooltip('show');  validated = false; }
        if ( $(email).val() == undefined || $(email).val() == "") { $(email).parent().addClass('has-error'); $(email).tooltip('show');  validated = false; }
        if ( $(phone).val() == undefined || $(phone).val() == "") { $(phone).parent().addClass('has-error'); $(phone).tooltip('show');  validated = false; }

        if (validated) {
            message =
                    "Имя: <strong>" + $(name).val() + "</strong><br>" +
                    "E-mail: <strong>" + $(email).val() + "</strong><br>" +
                    "Телефон: <strong>" + $(phone).val() + "</strong><br>" +
                    "<hr>";
            sendAjax(tip,message,this);
            yaCounter30739018.reachGoal('zayavka');

            setTimeout(function(){
                document.location.href = "/promo1/#thankyou";
                //document.location.href = "http://www.palla.su/katalog";
            }, 1000);

            $('#modalResponder').modal();

        } else { $('#mainscreenform input[data-value="phone"]').parent().addClass('has-error'); }
    });

    $(' #modal_callback .sender').click(function(){
        var validated = true;

        var name = $(this).parent().parent().find("input.name");
        var email = $(this).parent().parent().find("input.email");
        var phone = $(this).parent().parent().find("input.phone");
        var tip = $(this).attr('data-value-id');
        console.log(tip);

        var message = "";
        //console.log(phone);
        if ( $(name).val() == undefined || $(name).val() == "") { $(this).parent().addClass('has-error'); $(name).tooltip('show');  validated = false; }
        if ( $(email).val() == undefined || $(email).val() == "") { $(this).parent().addClass('has-error'); $(email).tooltip('show');  validated = false; }
        if ( $(phone).val() == undefined || $(phone).val() == "") { $(this).parent().addClass('has-error'); $(phone).tooltip('show');  validated = false; }

        if (validated) {
            message =
                "Имя: <strong>" + $(name).val() + "</strong><br>" +
                "E-mail: <strong>" + $(email).val() + "</strong><br>" +
                "Телефон: <strong>" + $(phone).val() + "</strong><br>" +
                "<hr>";

            sendAjax(tip,message,this);
            yaCounter30739018.reachGoal('zayavka');
            document.location.href = "/promo1/#thankyou";

            $('#modalResponder').modal();
        } else { $('#mainscreenform input[data-value="phone"]').parent().addClass('has-error'); }
    });

    $('#modal_catalog .sender').click(function(){
        var validated = true;

        var name = $(this).parent().parent().find("input.name");
        var email = $(this).parent().parent().find("input.email");
        var phone = $(this).parent().parent().find("input.phone");

        var tip = $(this).attr('data-value-id');
        console.log(tip);

        var message = "";
        //console.log(phone);
        if ( $(name).val() == undefined || $(name).val() == "") { $(name).parent().addClass('has-error'); $(name).tooltip('show');  validated = false}
        if ( $(email).val() == undefined || $(email).val() == "") { $(email).parent().addClass('has-error'); $(email).tooltip('show');  validated = false}
        if ( $(phone).val() == undefined || $(phone).val() == "") { $(phone).parent().addClass('has-error'); $(phone).tooltip('show');  validated = false}
        if (validated) {
            $('#catalog_download_body').removeClass('hidden');
            $('#catalog-controls').addClass('hidden');

            message ="Имя: <strong>" + $(name).val() + "</strong><br>" +
                "E-mail: <strong>" + $(email).val() + "</strong><br>" +
                "Телефон: <strong>" + $(phone).val() + "</strong><br>" + "<hr>";
            sendAjax(tip,message,this);
            yaCounter30739018.reachGoal('zayavka');
            document.location.href = "/katalog/";
        } else { }
    });


    $('.form-question .sender').click(function(){
        var validated = true;

        var name = $(this).parent().parent().find("input.name");
        var phone = $(this).parent().parent().find("input.tel");
        var email = $(this).parent().parent().find("input.email");
        var text = $(this).parent().parent().find("textarea.message");
        var tip = $(this).attr('data-value-id');
        console.log(tip);

        var message = "";
        //console.log(phone);
        if ( $(name).val() == undefined || $(name).val() == "") { $(name).parent().addClass('has-error'); $(name).tooltip('show');  validated = false}
        if ( $(phone).val() == undefined || $(phone).val() == "") { $(phone).parent().addClass('has-error'); $(phone).tooltip('show');  validated = false}
        if ( $(email).val() == undefined || $(email).val() == "") { $(email).parent().addClass('has-error'); $(email).tooltip('show');  validated = false}
        if ( $(text).val() == undefined || $(text).val() == "")   { $(text).parent().addClass('has-error'); $(text).tooltip('show');  validated = false}
        if (validated) {
            message =
                "Имя: <strong>" + $(name).val() + "</strong><br>" +
                "Телефон: <strong>" + $(phone).val() + "</strong><br>" +
                "e-mail: <strong>" + $(email).val() + "</strong><br>" +
                "сообщение: <strong>" + $(text).val() + "</strong><br>" +
                    "<hr>";
            sendAjax(tip,message,this);
            yaCounter30739018.reachGoal('zayavka');
            document.location.href = "/promo1/#thankyou";
        };
    });


    // metrika callbacks
   $('.yametrika').click(function(){
       var callbackid = $(this).attr('data-metrika-id');
       yaCounter30739018.reachGoal(callbackid);
       console.log('Reach goal: ' + callbackid);
   });

});



