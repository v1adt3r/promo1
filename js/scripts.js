

$(function(){
    $('#main-nav-data ul li a').click(function(){
        var href = $(this).attr('href');
        var offset = $(href).offset();
        console.log(offset.top);
        $("html, body").animate({ scrollTop: (offset.top - 40) }, "slow");
        return false;
    });

    $('.navbar-brand').click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });


    $('input[type="tel"]').mask('+7 (999) 999-99-99');
    $('input').focus(function(){
        $(this).parent().removeClass('has-error');
    });

    // production slider
    $('.production-slider').slick({
        infinite: true
    });

    // products sliders
    $('.items-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '30px'
    });

    // testimonials sliders
    $('.testimonials-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '60px'
    });

    // fancybox

    $(".fancybox").fancybox({
        openEffect	: 'none',
        closeEffect	: 'none'
    });

    skrollr.init();
    new WOW().init();
    $("img.lazy").lazy({
        effect: "fadeIn",
        effectTime: 1500});

    adjustRows();
    $(window).resize(function(){adjustRows();});

    $('#first_countdown').ResponsiveCountdown({
        target_date:"2020/1/1 00:00:00",
        show_dd: false,
        text_color:"rgb(0,0,0)",
        text_blur:0,
        hours_long:"часов",hours_short:"ч",
        mins_long:"минут",mins_short:"мин",
        secs_long:"секунд",secs_short:"сек"
    });

    function adjustRows(){
        var h = $(window).height();
        var rows = $('.screen');
        for (var i=0; i < rows.length; i++ ){
            $(rows[i]).css('min-height', h+'px');
        }

        var promo = $('#promo');
        var promo_parent = $('#first');

        $(promo).css('margin-top', ( $(promo_parent).height() - $(promo).height()) / 16 );
        $(promo).css('min-height', ( $(promo_parent).height() / 2.5) );
        //$(promo).css('padding-top', ( $(promo).height() / 10) );
    }
});


