<?php
$utp = "";

if (isset($_GET["utm_term"])) {
 $arguments = $_GET;
 //var_dump($arguments);
 //$utp = ($arguments["utm_term"]);
}


$multiplicator = 4;

$date = new stdClass();

$date->monthes = array("январе","феврале","марте","апреле","мае","июне","июле","августе","сентябре","октябре","ноябре","декабре");
$date->monthes_rod = array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
$date->day = (int)date("d");
$date->month = (int)date("m");
$date->year = (int)date("Y");
$date->days_total = cal_days_in_month(CAL_GREGORIAN,(int)$date->month,(int)date("Y"));

if ($date->day + $multiplicator > $date->days_total){
    $date->month = ($date->month) % 12;
}

$dir = "img/production/";
$files = scandir($dir);


?>

<!DOCTYPE html>
<html>
<head>
    <title>Palla</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTEwMjgxO2xzdGF0cy5yZWZlcmVyKCl9O3ZhciBqcXVlcnlfbHN0YXRzPWZ1bmN0aW9uKCl7alFzdGF0Lm5vQ29uZmxpY3QoKTtsb2Fkc2NyaXB0KCJzdGF0c19hdXRvLmpzIixpbml0X2xzdGF0cyl9O2xvYWRzY3JpcHQoImpxdWVyeS0xLjEwLjIubWluLmpzIixqcXVlcnlfbHN0YXRzKQ=="></script></noindex>

</head>
<body id="skrollr-body">

<div class="hidden" title="Заказать звонок"
     data-0="background:#00cd06; display:block; position: fixed; bottom: 5%; right: 5%; width: 90px; height: 90px; opacity: 1;"
     id="callback" data-toggle="modal" data-target="#modal_callback">
    <i style="position: absolute; top: 18px; left: 21px" class="fa fa-phone fa-4x"></i>
</div>

<div class="container-fluid screen" id="first">
    <div class="container">
        <nav class="navbar navbar-default" id="main-nav"
             data-0="background:none; position: relative;box-shadow:0 0px 0px rgba(255,255,255,1);"
             data-150="background:white; position: fixed; z-index: 10; width:100%; left:0;top:0;margin:0;box-shadow:0 1px 6px rgba(0,0,0,0.36);">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="https://www.palla.su/promo1">
                        <img class="img-responsive lazy" width="66" alt="Palla.su" data-src="img/logotype.png" src="">
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-data">
                        <span class="sr-only">Открыть меню</span>
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <div class="collapse navbar-collapse bg-light mb-md" id="main-nav-data">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#advantages">Преимущества</a></li>
                        <li><a href="#production">Наше производство</a></li>
                        <li><a href="#assortiment">Ассортимент</a></li>
                        <li><a href="#whowewantto">С кем работаем</a></li>
                        <li><a href="#our-conditions">Условия</a></li>
                        <li><a href="#howwework">Как мы работаем</a></li>
                        <li><a href="#sertificates">Сертификаты</a></li>
                        <li><a href="#testimonials">Отзывы</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="container">
      <div class="row" id="promo">

        <div class="col-md-2 bg-light ">
            <p class="h3" style="margin: 0" >
                <a href="tel:8(800)555-17-03" data-metrika-id="telefon" class="yametrika">
                    <span class="lptracker_phone">8 (800) 555-17-03</span>
                </a></p>
            звонок бесплатный
        </div>

              <div class="col-md-6 col-md-offset-4 text-center">
                  <img data-src="img/logotype.png" class="center-block lazy bg-light" src="">
                  <p class="h1"><?php //print($utp); ?></p>
                  <p class="h2 bg-light">Женская одежда <br> <span style="border-bottom: red dotted 2px; font-family: 'pfs'; font-size: 1.3em; color: #dd0000; bottom: -1px; position: relative">оптом</span> <br> от производителя</p>


                  <div class="push-top">
                      <p class="h3 bg-light">Получите доступ к каталогу с оптовыми ценами!</p>
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="form-inline  form-consult d-flex">

                                      <div class="col-md-4 col-md-offset-8">
                                          <div class=" form-group mb-md">
                                              <i class="fa fa-user"></i>
                                              <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
                                          </div>

                                          <div class="form-group mb-md">
                                              <i class="fa fa-envelope"></i>
                                              <input type="email" class="form-control email" placeholder="Ваш e-mail*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
                                          </div>

                                          <div class="form-group mb-md">
                                              <i class="fa fa-phone"></i>
                                              <input type="tel" class="form-control phone" placeholder="Ваш телефон*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
                                          </div>

                                          <button class="btn btn-primary btn-lg sender"
                                                  data-value="Получение консультации" data-value-id="consult">
                                              <i style="left: -5px" class="fa fa-paper-plane"></i> Получить доступ</button>
                                      </div>





                                  </div>
                              </div>
                          </div>
                      </div>
              </div>

          <div class="push-top">&nbsp;</div>
      </div>
    </div>

    <div class="container">
      <div class="clearall btn-promo bg-light promo-bottom-text text-center">
          <small>* отправка сообщения ни к чему Вас не обязывает! Вы сможете оценить выгодность предложенных
              нами условий сотрудничества и принять решение! Мы не передаем Ваши контактные данные третьим лицам.</small>
      </div>
    </div>
</div>

<div id="spring_action">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <img data-0="transform: scale(0.3)" data-500="transform: scale(1)" style="margin-top: -35px" data-src="img/gift2.png" class="img-responsive lazy" src="">
            </div>

            <div class="col-md-8 items">

                <p class="h1 text-centered text-uppercase" style="color: #d30404">Акция до конца месяца!</p>
                <p class="h2 text-centered">Заключите договор до <?php print($date->days_total." ".$date->monthes_rod[$date->month-1]) ?> и Вы получите:</p>
                <div class="col-md-12 mb-lg">
                    <hr class="clearall">
                    <div class="livicon pull-left" style="margin: -5px 15px  7px 0px" data-size="80" data-n="map" data-c="#d30404" data-hc="#d30404"></div>
                    <p class="h2">Гарантия возврата денежных средств!</p>
                    <p class="h4">Если товар не пойдет, мы вернем вам деньги! <br> Только для ИП и ООО!  </p>

                </div>

                <div class="col-md-12 mb-lg">
                    <div class="livicon pull-left" style="margin: 7px 15px 0 0" data-size="90" data-n="truck" data-c="#d30404" data-hc="#d30404"></div>
                    <p class="h2">Бесплатная доставка до транспортной компании</p>

                </div>

                <div class="col-md-12">
                    <div class="livicon pull-left" style="margin: 7px 15px 0 0" data-size="90" data-n="shuffle" data-c="#d30404" data-hc="#d30404"></div>
                    <p class="h2 mb-xlg">При заказе на сумму более 30 000 рублей Вы получите скидку:</p>
                    <p class="h2 discount">
                          от 30 000 = <span class="light-red-color">скидка 2%</span> <br>
                          от 50 000 = <span class="light-red-color">скидка 5%</span> <br>
                          от 100 000 = <span class="light-red-color">скидка 7%</span> <br>
                          от 150 000 = <span class="light-red-color">скидка 10%</span> <br>
                        </ul>
                    </p>
                </div>

                <div class="push-top">&nbsp;</div>
            </div>
        </div>
    </div>
</div>

<div class="">

</div>

<div class="sunburst">
    <div class=" text-center">
        <p class="h1 altfont">Получите доступ к каталогу с оптовыми ценами!</p>
        <p class="h4 altfont">Оставьте Ваши контактные данные и наш менеджер свяжется с Вами в течение 15 минут!*</p>

        <div class="form-inline push-top form-special">
            <div class=" form-group">
                <i class="fa fa-user"></i>
                <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
            </div>
            <div class=" form-group">
                <i class="fa fa-envelope"></i>
                <input type="email" class="form-control email" placeholder="Ваш e-mail*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
            </div>
            <div class=" form-group">
                <i class="fa fa-phone"></i>
                <input type="tel" class="form-control phone" placeholder="Ваш телефон*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
            </div>
            <button class="form-control btn btn-primary sender" data-value="Специальные условия" data-value-id="special">Получить доступ</button>
        </div>
        <p class="push-top"><small>* В связи с большим количеством заявок время ожидания может быть увеличено до 1 рабочего дня.</small></p>
    </div>
</div>



<div id="production-types">
    <div class="container">
        <div class="row">
            <p class="h1 text-center push-top text-uppercase" style="margin-bottom: 1.618em">Наша продукция</p>
            <div class="col-md-3">
                <div class="item" data-toggle="modal" data-target="#modal_catalog">
                    <img src="https://palla.su/image/cache/catalog/products/bruki/271-618/271-618%20%281%29-1064x1600.jpg" class="img-responsive">
                    <p class="h3 text-center">Брюки</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item" data-toggle="modal" data-target="#modal_catalog">
                    <img src="https://palla.su/image/cache/catalog/products/ubki/031-302-58/031-302-58%20%281%29-1064x1600.jpg" class="img-responsive">
                    <p class="h3 text-center">Юбки</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item" data-toggle="modal" data-target="#modal_catalog">
                    <img src="https://palla.su/image/cache/catalog/products/platya/100-828/100-828%D0%94%20%281%29-1064x1600.jpg" class="img-responsive">
                    <p class="h3 text-center">Платья</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item" data-toggle="modal" data-target="#modal_catalog">
                    <img src="https://palla.su/image/cache/catalog/products/bluzki/41-357/41-357%D0%94%20%282%29-1064x1600.jpg" class="img-responsive">
                    <p class="h3 text-center">Блузы</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php


$assortiment = array();

/* $db_name = "palla";
$db_user = "root";
$db_password = "";
$db_host = "localhost"; */

$db_name = "u0040958_default";
$db_user = "u0040958_default";
$db_password = "u00S4l09P5j";
$db_host = "localhost";

try {
    $connector = new PDO("mysql:host=$db_host;dbname=$db_name",$db_user,$db_password);
    $connector->query('SET NAMES `UTF8`');
    print("<div id='slider_output' class='hidden'></div>");
    $products = array(1844, 1865, 2041, 2042, 303, 1157, 1127, 1099, 1816, 1858, 1163, 1830, 1851, 1169, 1870);

    foreach ($products as $product){
        $id = $product;
        $query = "SELECT * FROM `oc_product` WHERE `product_id`='$id' LIMIT 1";

        $p = $connector->query($query);
        $p->setFetchMode(PDO::FETCH_OBJ);
        $res = $p->fetchAll();

        $description = "SELECT `name` FROM `oc_product_description` WHERE `product_id`='$id' LIMIT 1";
        $p = $connector->query($description);
        $p->setFetchMode(PDO::FETCH_OBJ);
        $res_ = $p->fetchAll();

        $res[0]->description = $res_[0];
        array_push($assortiment, $res[0]);
    }

} catch (Exception $e){

}

/*



*/
//var_dump($assortiment[0]);

?>

<div id="assortiment">
    <div class="container">
        <div class="row">
            <p class="h1 text-center push-top text-uppercase">Зимняя коллекция Palla <?= $date->year ?></p>
            <div class="items-slider push-top" id="items-slider">
                <?php

                 foreach ($assortiment as $assortiment_item){
                    if ((int)$assortiment_item->price == 0){$assortiment_item->price = "Уточняйте цену у менеджера!";} else {$assortiment_item->price = (int)$assortiment_item->price." <i class='fa fa-rouble'></i>";}
                    print("<div class='item'>
                    <a href='https://palla.su/image/".$assortiment_item->image."' class='fancybox'>
                        <img src='https://palla.su/image/".$assortiment_item->image."' class='img-responsive  image' src='' alt=''>
                                <span class='product-description'>
                                    <span class='h4'>".$assortiment_item->description->name."</span>

                                </span>
                    </a>
                </div>");
                }
                ?>
            </div>
        </div>

        <div class="col-md-7 hidden">
            <p class="h4 text-right hidden">... а также более 300 наименований продукции в <strong>осеннем каталоге Palla <?= $date->year ?></strong>.</p>
        </div>
        <div class="col-md-12">
            <button class="btn btn-lg btn-primary center-block clothed altfont" data-toggle="modal" data-target="#modal_catalog">
                <i class="livicon" data-size="25" data-n="download" data-color="white" data-hc="yellow" style="top: 8px; position: relative"></i>
                Просмотреть каталог зимней продукции Palla <?= $date->year ?>
            </button>
        </div>
    </div>
</div>


<div id="advantages" class="screen"
     data-1000="transition: all 1s ease; background-position: 85% 0px, 0px 0px;"
     data-1100="transition: all 1s ease; background-position: 100% 0px,  500 0px;"
      >

    <div class="container">
        <div class="row">
            <p class="h1 text-center text-uppercase">Преимущества работы с нами</p>

            <div class="col-md-8 items">
                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="credit-card" data-c="#ec604f"></div>
                    <p class="h4">Оптовые цены. Минимальный заказ от 20 000 рублей.</p>
                </div>
                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="certificate" data-c="#ec604f"></div>
                    <p class="h4">Высокое качество изделий. Весь товар сертифицирован.</p>
                </div>
                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="thumbs-up" data-c="#ec604f"></div>
                    <p class="h4">Производится в России. Без таможенных наценок.</p>
                </div>

                <div class="col-md-4 clearall wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="refresh" data-c="#ec604f"></div>
                    <p class="h4">Регулярное обновление каталога.</p>
                </div>

                <div class="col-md-4  wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="shopping-cart" data-c="#ec604f"></div>
                    <p class="h4">Более 5000 постоянных клиентов по всей России.</p>
                </div>

                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="linechart" data-c="#ec604f"></div>
                    <p class="h4">Неизменный спрос
                    на товар круглый
                    год.</p></div>

                <div class="col-md-4 clearall  wow fadeIn" data-delay="0.2s">
                  <div class="livicon center-block" data-size="50" data-n="tag" data-c="#ec604f"></div>
                  <p class="h4">Cкидка от суммы заказа до 10%</p>
                </div>

                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="globe" data-c="#ec604f"></div>
                    <p class="h4">Доставка по всей России</p>
                </div>

                <div class="col-md-4 wow fadeIn" data-delay="0.2s">
                    <div class="livicon center-block" data-size="50" data-n="trophy" data-c="#ec604f"></div>
                    <p class="h4">Более 20 лет опыта на рынке</p>
                </div>

            </div>

            <div class="col-md-4">
              <ul class="managers">
                <li class="item">
                  <img class="img-responsive center-block lazy" data-src="img/managers/olga.jpg" src="" alt="розничные магазины">
                  <p class="h4">Иванова <br> Ольга</p>
                </li>
                <li class="item ml-md">
                  <img class="img-responsive center-block lazy" data-src="img/managers/ekaterina.jpg" src="" alt="розничные магазины">
                  <p class="h4">Куракова <br> Екатерина</p>
                </li>
                <li class="item mt-lg">
                  <img class="img-responsive center-block lazy" data-src="img/managers/anna.jpg" src="" alt="розничные магазины">
                  <p class="h4">Трифонова <br> Анна</p>
                </li>
                <li class="item mt-lg ml-md">
                  <img class="img-responsive center-block lazy" data-src="img/managers/ekaterina2.jpg" src="" alt="розничные магазины">
                  <p class="h4">Шевченко <br> Екатерина</p>
                </li>
              </ul>
            </div>

            <div class="col-md-8 clearall">

                <p class="h2 text-center text-uppercase">Оставьте заявку на бесплатную консультацию</p>
                <p class="h4 text-center">Наш специалист свяжется с Вами и ответит на все вопросы!</p>
                <div class="form-inline text-center form-cooperation">
                        <div class="form-group">
                            <i class="fa fa-user"></i>
                            <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
                        </div>
                        <div class="form-group">
                            <i class="fa fa-envelope"></i>
                            <input type="email" class="form-control email" placeholder="Ваш e-mail*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
                        </div>
                        <div class="form-group">
                            <i class="fa fa-phone"></i>
                            <input type="tel" class="form-control phone" placeholder="Ваш телефон*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
                        </div>
                        <button class="form-control btn btn-primary sender" data-value="Заявка на сотрудничество" data-value-id="cooperation">Отправить заявку</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div  id="production">
    <div class="container">
        <div class="row">


            <div class="col-md-7">

                <div class="production-slider" id="production-slider" style="margin-top: -50px"
                     data-1800="transform: perspective(450px) rotateY(-1deg);"
                     data-2100="    transform: perspective(450px) rotateY(-1deg);">
                    <?php

                    for ($i=0; $i<count($files); $i++){
                        if ($files[$i] != "." && $files[$i] != ".."){
                            ?>

                            <div><img class='img-responsive img-rounded' src='<?= "img/production/".$files[$i] ?>'></div>

                            <?php
                        }
                    }

                    ?>


                </div>


            </div>
            <div class="col-md-4 wow zoomIn">
                <p class="h1 text-uppercase">Наше производство</p>

                <div id="director_speech">
                    <p class="h4" style="margin-top: 4em">"Мы стремимся максимально удовлетворить запросы и пожелания наших
                        клиентов и партнеров, найти индивидуальный подход к каждому."</p>
                </div>
            </div>
        </div>



    </div>
</div>




<div id="our-conditions">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <img class="img-responsive center-block lazy" data-src="img/bg-condition.jpg" src="" alt="розничные магазины">
        </div>

        <div class="col-md-7">
          <p class="h1 text-center text-uppercase">Наши условия Вам понравятся</p>
          <p class="text-center h3 mb-xlg">При заказе на сумму более 30 000 рублей Вы получите скидку:</p>
          <p class="h2 discount mb-xlg">
                от 30 000 = <span class="light-red-color">скидка 2%</span> <br>
                от 50 000 = <span class="light-red-color">скидка 5%</span> <br>
                от 100 000 = <span class="light-red-color">скидка 7%</span> <br>
                от 150 000 = <span class="light-red-color">скидка 10%</span> <br>
              </ul>
          </p>


          <div class="row clearall">
              <div class="col-md-4 text-center item">
                  <div class="livicon center-block" data-size="80" data-n="connect" data-c="#ec604f" data-hc="#ec604f"></div>
                  <p class="h5">Работа напрямую с производителем одежды. Актуальный,
                      модный ассортимент женской одежды.</p>
              </div>

              <div class="col-md-4 text-center item">
                  <div class="livicon center-block" data-size="80" data-n="money" data-c="#ec604f" data-hc="#ec604f"></div>
                  <p class="h5">Выгодные цены относительно аналогичных предложений конкурентов (даже не учитывая оптовую скидку).</p>
              </div>

              <div class="col-md-4 text-center item">
                  <div class="livicon center-block" data-size="70" data-n="tablet" data-c="#ec604f" data-hc="#ec604f"></div>
                  <p class="h5">Вся информация в удобном формате: описание всех моделей,
                      фотографии в отличном качестве, оперативно обновляемые прайс-листы на все товары.</p>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="text-center sunburst ">
    <p class="h2 text-center text-uppercase">Получите доступ к каталогу с оптовыми ценами!</p>
    <div class="form-inline  form-cooperation">
        <div class="form-group form-group-lg">
            <i class="fa fa-user"></i>
            <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
        </div>
        <div class="form-group form-group-lg">
            <i class="fa fa-envelope"></i>
            <input type="email" class="form-control email" placeholder="Ваш e-mail*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
        </div>
        <div class="form-group form-group-lg">
            <i class="fa fa-phone"></i>
            <input type="tel" class="form-control phone" placeholder="Ваш телефон*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
        </div>
        <button class="btn btn-primary btn-lg sender" data-value="Заявка на сотрудничество" data-value-id="cooperation">Получить доступ</button>
    </div>
    <p class="h5 text-center">Наши менеджеры свяжутся с вами в течение 15 минут*</p>
    <p class="push-top"><small>* В связи с большим количеством заявок время ожидания может быть увеличено до 1 рабочего дня.</small></p>
</div>

<div id="whyprofit" class="hidden">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="h1 text-right">Почему с нашей компанией Вы можете <strong
                        data-4200="transform: scale(0.44); display: inline-block;"
                        data-4400="transform: scale(1);  display: inline-block;"
                        style="font-size: 1.33em; color: #ec604f">удвоить</strong> продажи?</p>
                <p class="h4 text-right">Компания Palla производит яркие и отлично продающиеся коллекции женской одежды!
                    Наши клиенты - владельцы магазинов женской одежды, которые не могут себе позволить
                    больших остатков на складе. Только у нас 100% моделей это проверенные маркетинговыми
                    исследованиями хиты продаж. Уверенный спрос на местах это доказывает!</p>
            </div>
            <div class="col-md-4">
                <img style="margin-top: -30px; margin-bottom: -30px"
                     data-4900="transform: scale(0.6) rotate(0deg);" data-5300="transform: scale(1) rotate(-21deg);"

                     class="img-responsive lazy" data-src="img/rocket-big.png" src="">
            </div>
        </div>
    </div>
</div>

<div>
    <div class="container push-top" id="howwework">
        <div class="row">
            <p class="h2 text-center text-uppercase" >Как мы работаем</p>
            <img data-src="img/workflow.jpg" src="" class="img-responsive lazy">


                <div class="col-md-4 text-center">
                    <p class="h4"> Вы оформляете заявку на сайте или по телефону.</p>
                </div>
                <div class="col-md-4 text-center">
                    <p class="h4"> Наш менеджер связывается с Вами и уточняет детали сотрудничества.</p>
                </div>
                <div class="col-md-4 text-center">
                    <p class="h4"> Вы оформляете и оплачиваете заказ, мы высылаем его Вам транспортной компанией. Оплата при получении.</p>
                </div>


        </div>
    </div>
</div>

<div hidden class="form-inline text-center sunburst">
    <p class="h2 text-center text-uppercase">Начать сотрудничество</p>
    <div class="form-group-lg form-cooperation">

        <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
        <input type="email" class="form-control email" placeholder="Ваш e-mail*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
        <input type="tel" class="form-control phone" placeholder="Ваш телефон*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">

        <button class="btn btn-primary btn-lg sender" data-value="Заявка на сотрудничество" data-value-id="cooperation">Оставить заявку</button>
    </div>
    <p class="h5 text-center">Наши менеджеры свяжутся с вами в течение 15 минут*</p>
    <p class="push-top"><small>* В связи с большим количеством заявок время ожидания может быть увеличено до 1 рабочего дня.</small></p>
</div>


<div id="sertificates">
    <div class="container">
        <div class="row">


            <div class="col-md-6 push-top">

                <div class="col-md-3">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/sert01.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/sert01.jpg" src="">

                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/sert02.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/sert02.jpg" src="">

                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/sert03.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/sert03.jpg" src="">

                        </a>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/sert04.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/sert04.jpg" src="">

                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl7a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl7a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl1a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl1a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl2a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl2a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl3a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl3a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl4a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl4a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl5a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl5a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <a class="fancybox" rel="gallery_sertficates" href="img/sertificates/dipl6a.jpg">
                            <img class="img-responsive lazy" data-src="img/sertificates/dipl6a.jpg" src="">
                            <i class="fa fa-search-plus"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <p class="h1 text-uppercase">Сертификаты и награды</p>

                <p class="lead">Продукция компании многократно отмечена профессиональными наградами в
                    номинации «Лучший товар России».</p>

                <p class="lead bigger">Ежегодно дизайнеры марки разрабатывают до 100 новых моделей, всего же в каталоге фирмы более 1000 позиций.</p>

                <button class="btn btn-lg btn-primary clothed altfont" data-toggle="modal" data-target="#modal_catalog">
                    <i class="livicon" data-size="25" data-n="download" data-color="white" data-hc="yellow" style="top: 8px; position: relative"></i>
                    Просмотреть каталог осенней продукции Palla <?= $date->year ?>
                </button>
            </div>
        </div>
    </div>
</div>


<div id="testimonials">
    <div class="container" id="testimonials-content">
        <div class="row">
            <p class="h1 text-center text-uppercase">Что говорят о нас наши партнеры?</p>

            <div class="testimonials-slider">
                <div>
                    <div class="item">
                        <p>«Я уже на протяжении нескольких лет заказываю вашу продукцию и могу сказать с уверенностью,
                            что это лучшее соотношение цены и качества, при самом доброжелательном отношении к клиенту и
                            его пожеланиям, со стороны сотрудников компании. Заявки обрабатываются оперативно, качество
                            товара всегда превосходное. Огромное спасибо за ваш труд.»</p>
                        <p class="h4">Антонина, владелец бизнеса, г.Екатеринбург:</p>
                    </div>
                </div>
                <div>
                    <div class="item">
                        <p>«В наших магазинах очень пользуются спросом ваши новинки. Радует, что такая хорошая классическая
                        одежда производится в России. Спасибо за своевременное информирование о новинках, акциях и
                        распродажах. Хорошие условия сотрудничества. Буду заказывать ещё!»</p>
                        <p class="h4">Ольга Юрьева, владелец бизнеса, г.Новосибирск:</p>
                    </div>
                </div>

                <div>
                    <div class="item">
                        <p>«Добрый день! Хотела бы выразить благодарность за оперативность и отзывчивость ваших
                        сотрудников. Я довольна качеством работы и качеством доставки. Продукция как всегда
                        «на отлично». Все модельки продались, покупатели спрашивают о новых поступлениях.»</p>
                        <p class="h4">Наталья, г.Челябинск:</p>
                    </div>
                </div>
                <div>
                    <div class="item">
                        <p>«В этом магазине можно подобрать и брюки, и юбки, и шорты, и бриджи. Можно наткнуться и на
                            некрасивые модели, но  это в любых магазинах на вкус и цвет как говорится... Мне очень
                            нравится этот производитель, в нём я покупаю продукцию, которую берут в моём магазине.
                            Рекомендую. »</p>
                        <p class="h4">Татьяна, владелец бизнеса, г.Санкт-Петербург:</p>
                    </div>
                </div>

                <div>
                    <div class="item">
                        <p>«Чем меня привлекает одежда этого бренда? Очень изящные женственные линии, приталенный силуэт,
                        яркие красивые вещи. Модели делятся на множество категорий, одна из них предназначена для
                        молодежи, тут могут быть более дерзкие вещи, а одна часть вещей - больше для взрослых женщин.
                        Среди них всегда есть модели больших размеров. »</p>
                        <p class="h4">Алёна, г.Волгоград:</p>
                    </div>
                </div>
                <div>
                    <div class="item">
                        <p>«Неоднократно покупаю одежду у этого производителя. Не могу сказать, что весь мой магазин
                        состоит из одежды этой марки, но все, же их много. Как и у любого производителя, у Palla
                        огромный модельный ряд на все времена года. </p>
                        <p>Поэтому трудностей с выбором нет.
                        По поводу цен могу сказать, что одежда, сшитая на производстве, имеет высокое
                        качество и окупает себя в разы. За годы работ с этим производителем, у меня
                        сформировались постоянные клиенты. Советую!»</p>
                        <p class="h4">Елена, г.Хабаровск:</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div  style="background: #31708f;">
    <div class="container">
        <div class="row">
            <div class="col-md-8" id="question_girl">
                <img data-src="img/pretty_girl_laptop.jpg" src="" class="img-responsive lazy">
            </div>

            <div class="col-md-4">
                <p class="h1 text-uppercase" style="color: #ffffff">Остались вопросы?</p>

                <div class="form push-top form-question">

                    <div class="form-group">
                        <input type="text" class="form-control name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control tel" placeholder="Ваш телефон"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control email" placeholder="Ваш email"  data-toggle="tooltip" title="Пожалуйста, введите Ваш e-mail">
                    </div>
                    <div class="form-group">
                        <textarea  class="form-control message" placeholder="Ваше сообщение" data-toggle="tooltip" title="Пожалуйста, введите Ваше сообщение"></textarea>
                        <small class="h6" style="color: #ffffff">Ваши данные в безопасности и не передаются третьим лицам.</small>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary sender" data-value-id="form">Отправить сообщение</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3"><img class="lazy" data-src="img/logotype.png" src="">
                <div><small>Оптовое производство женской одежды</small></div>
            </div>


            <div class="col-md-3"><p class="h4">Реквизиты</p>
                <div><strong>ИП Гричаник</strong> Виктор Николаевич</div>
                <div><strong>ОГРН </strong> 307502209500040</div>

            </div>
            <div class="col-md-4"><p class="h4">Контакты</p>
                <div class="color-white">
                    <a class="h2 yametrika" data-metrika-id="telefon" href="tel:8(800)555-17-03">
                        <span class="lptracker_phone">8 (800) 555-17-03</span></a>
                </div>
                <div><small>140411, Московская область, г. Коломна, Озерский проезд, д. 1</small></div>
            </div>
        </div>
    </div>
</footer>

</body>

<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/slick.css" />
<link rel="stylesheet" href="css/slick-theme.css" />
<link rel="stylesheet" href="css/jquery.fancybox.css" />
<link rel="stylesheet" href="css/animate.css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script src="js/jquery.js"></script>
<script src="js/raphael.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/livicons-1.4.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/skrollr.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery.lazy.min.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/ajaxformsender.js"></script>
<script src="js/jquery.responsive_countdown.min.js"></script>
<script src="js/scripts.js"></script>
<!-- <script src="js/snowstorm.js"></script> -->

<?php

if ($_SERVER['HTTP_HOST'] != 'palla.landing'){ ?>
    <!-- Yandex.Metrika informer --><a href="https://metrika.yandex.ru/stat/?id=30739018&amp;from=informer"target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/30739018/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:30739018,lang:'ru'});return false}catch(e){}" /></a><!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30739018 = new Ya.Metrika({ id:30739018, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/30739018" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<?php } ?>

</html>

<?php include_once('forms.php'); ?>
