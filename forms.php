<!-- callback modal -->
<div class="modal fade" id="modalResponder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i> </span></button>
                <h4 class="modal-title" id="modal_callback_label">Спасибо!</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="h1 text-center">Спасибо за Вашу заявку!</p>
                        <p class="h2"> В ближайшее время с Вами свяжется менеджер.</p>
                        <p class="h3">Также вы можете самостоятельно ознакомиться с полным ассортиментом и оптовыми ценами в нашем интернет-магазине.</p>
                        <p class="h4"> Доступ к оптовым ценам Вы сможете получить после регистрации.</p>
                        <hr>
                        <a target="_blank" class="btn btn-primary btn-lg center-block" href="http://www.palla.su/katalog">Перейти</a>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- callback modal -->
<div class="modal fade" id="modal_callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i> </span></button>
                <h4 class="modal-title" id="modal_callback_label">Заказать обратный звонок</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group-lg text-center">
                            <label><i class="fa fa-user"></i> Введите Ваше имя</label>
                            <input class="form-control text-center name" type="text" data-type="name" placeholder="Ваше имя"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
                        </div>
                        <div class="form-group-lg text-center">
                            <label><i class="fa fa-phone"></i> Введите Ваш номер телефона</label>
                            <input class="form-control text-center phone" type="tel" data-type="phone" placeholder="Ваш номер телефона"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
                        </div>
                        <div class="form-group-lg text-center">
                            <label><i class="fa fa-envelope"></i> Введите Ваш e-mail</label>
                            <input class="form-control text-center email" type="email" data-type="email" placeholder="Ваш email"  data-toggle="tooltip" title="Пожалуйста, введите Ваш e-mail">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="push-top text-center">Наши менеджеры свяжутся с Вами в течение 15 минут* и ответят на все Ваши вопросы!</p>

                        <button type="button" class="push-top btn btn-primary btn-lg center-block btn-block altfont sender" data-value-id="callback">Перезвоните мне!</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p class="text-center"> <small>* В связи с большим количеством заявок время ожидания может быть увеличено до 1 рабочего дня.</small></p>
            </div>
        </div>
    </div>
</div>

<!-- catalog modal -->
<div class="modal fade" id="modal_catalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-close"></i> </span></button>
                <h4 class="modal-title" id="modal_callback_label">Получить оптовый каталог Palla</h4>
            </div>
            <div class="modal-body">
                <div id="catalog-controls">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group-lg text-center">
                                <label><i class="fa fa-user"></i> Введите Ваше имя</label>
                                <input class="form-control text-center name" type="text" data-type="tel" placeholder="Иван Иванов"  data-toggle="tooltip" title="Пожалуйста, введите Ваше имя">
                            </div>
                            <div class="form-group-lg text-center">
                                <label><i class="fa fa-envelope"></i> Введите Ваш адрес email</label>
                                <input class="form-control text-center email" type="email" data-type="tel" placeholder="i.ivanov@mail.ru*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш email">
                            </div>
                            <div class="form-group-lg text-center">
                                <label><i class="fa fa-phone"></i> Введите Ваш номер телефона</label>
                                <input class="form-control text-center phone" type="tel" data-type="tel" placeholder="+7 (999) 123-45-67*"  data-toggle="tooltip" title="Пожалуйста, введите Ваш телефон">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="push-top">
                                <p class="h3">
                                    Чтобы посмотреть весь ассортимент и оформить заказ, оставьте свои
                                    данные и перейдите в наш интернет-магазин!</p>
                            </div>
                            <button type="button" class="btn btn-primary push-top btn-lg center-block btn-block altfont sender" data-value-id="catalog">Перейти в оптовый каталог!</button>
                        </div>
                    </div>
                </div>


                <div id="catalog_download_body" class="hidden animated zoomIn push-top">
                    <p class="h3 text-center altfont">Сейчас Вы будете перенаправлены в каталог осенней коллекции Palla 2015</p>
                    <a href="/katalog/?filter_ocfilter=3:8" class="altfont btn btn-lg btn-success center-block yametrika" data-metrika-id="katalog">Если этого не случилось - нажмите здесь</a>
                </div>
            </div>
            <div class="modal-footer push-top">
                <p class="push-top text-center">Ваши данные конфиденциальны и не передаются третьим лицам.</p>
            </div>
        </div>
    </div>
</div>

<div id="responder">
    <div class="container">
        <div class="row">
            <i class="fa fa-2x fa-close pull-right" id="dissmissResponder"></i>
            <p class="h1 text-center">Спасибо за обращение!</p>
            <p class="h2 text-center">Наши менеджеры свяжутся с Вами в течение 15 минут</p>
        </div>
    </div>
</div>